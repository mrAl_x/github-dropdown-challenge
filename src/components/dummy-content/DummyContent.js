import React from 'react';

// Styles
import styles from './DummyContent.css';

/**
 * I serve only to respect the expected layout 😇
*/
const DummyContent = () => (
	<ul>
		<li className={ styles.dummyItem }>
			Some other content here
		</li>
		<li className={ styles.dummyItem }>
			Some other content here
		</li>
	</ul>
);

export default DummyContent;
