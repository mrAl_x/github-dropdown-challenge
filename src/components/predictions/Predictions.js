import React from 'react';
import PropTypes from 'prop-types';

import PredictionsList from './predictions-list/PredictionsList.js';

// Styles
import styles from './Predictions.css';

/**
 * Renders the predictions box title and list container
*/
const Predictions = (props) => {
	const updateA11yCurrentHighlight = (username) => {
		props.updateA11yCurrentHighlight(username);
	};

	return (
		<div className={ styles.Predictions }>
			<h2 className={ styles.title }>Github Users</h2>
			{
				(props.searchResults && props.searchResults.length > 0) ? <PredictionsList
					searchQuery={ props.searchQuery }
					searchResults={ props.searchResults }
					selectedListItemIndex={ props.selectedListItemIndex }
					isLinkClicked={ props.isLinkClicked }
					updateA11yCurrentHighlight={ updateA11yCurrentHighlight } /> :
					<p className={ styles.noResults }>
						No results
					</p>
			}
		</div>
	);
};

Predictions.propTypes = {
	searchResults: PropTypes.array.isRequired,
	searchQuery: PropTypes.string,
	selectedListItemIndex: PropTypes.number,
	isLinkClicked: PropTypes.bool,
	updateA11yCurrentHighlight: PropTypes.func,
};

export default Predictions;
