import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

// Styles
import styles from './PredictionItem.css';

const PredictionItem = (props) => {
	const {
		user,
		selectedListItemIndex,
		index,
		searchQuery,
	} = props;

	/**
	 * Take the string the user input on the text box and highlight it
	 * in the results' usernames
	*/
	const highlightQuery = (username) => {
		const index = username.toLowerCase().search(searchQuery.toLowerCase());

		if (index >= 0) {
			/**
			 * Take out the search query characters out of the string
			*/
			const newString = username.toLowerCase().substr(searchQuery.length, username.length);

			return (
				<span className={styles.username}
					aria-label={`${user.login}'s profile`}>
					<b>{ searchQuery }</b>{ newString }
				</span>
			);
		}

		return (
			<span className={ styles.username }
				aria-label={`${user.login}'s profile`}>
				{ username }
			</span>
		);

	};

	return (
		<li className={ styles.PredictionItem }>
			<a href={ user.html_url }
				className={ classnames(
					styles.link,
					{ [styles.link_hovered]: selectedListItemIndex === index }
			)}
				target="_blank">
				<img src={ user.avatar_url }
					alt={ user.login }
					className={ styles.avatar }
					role="presentation" />
				{
					highlightQuery(user.login)
				}
			</a>
		</li>
	);
}

PredictionItem.propTypes = {
	user: PropTypes.object.isRequired,
	searchQuery: PropTypes.string,
	selectedListItemIndex: PropTypes.number,
	index: PropTypes.number,
};

export default PredictionItem;
