import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import PredictionItem from '../prediction-item/PredictionItem.js'
// Styles
import styles from './PredictionsList.css';

class PredictionsList extends Component {
	constructor() {
		super();
		this.listRef = null;
	}

	/**
	 * Simulates a click on the highlighted item when the user presses the 'Enter' key
	 * and changes the url in the browser
	 * (Not my proudest code tbh 💩)
	*/
	componentWillReceiveProps(nextProps) {
		const { selectedListItemIndex, isLinkClicked } = this.props;
		// const userRef = this.listRef.children[selectedListItemIndex].children[0];

		/**
		 * Check if user changes the highlighted result
		*/
		if (this.props.selectedListItemIndex !== nextProps.selectedListItemIndex) {
			this.a11yhighlightedUser(nextProps.selectedListItemIndex);
		}

		if (isLinkClicked === true) {
			/**
			 * Get the highlighted item's href and change update the browser with the new url
			*/
			const targetUrl = this.listRef.children[selectedListItemIndex].children[0].href;

			window.location.href = targetUrl;
		}
	}

	render() {
		return (
			<ul className={ styles.PredictionsList }
				ref={(ref) => this.listRef = ref}>
				{
					this.props.searchResults && this.renderResults()
				}
			</ul>
		);
	}

	/**
	 * Renders the search results from the App's state
	*/
	renderResults = () => {
		const {
			searchResults,
			selectedListItemIndex,
			searchQuery
		} = this.props;

		return searchResults.map((user, index) => (
			<PredictionItem key={ index }
				user={ user }
				index={ index }
				searchQuery={ searchQuery }
				selectedListItemIndex={ selectedListItemIndex } />
		));
	}

	/**
	 * Finds the highlighted user's username by accessing the image's alt text and
	 * sends it to the App component
	*/
	a11yhighlightedUser = (index) => {
		const username = this.listRef.children[index].children[0].children[0].alt;

		this.props.updateA11yCurrentHighlight(username);
	}
}

PredictionsList.propTypes = {
	searchResults: PropTypes.array.isRequired,
	searchQuery: PropTypes.string,
	selectedListItemIndex: PropTypes.number,
	isLinkClicked: PropTypes.bool,
	updateA11yCurrentHighlight: PropTypes.func,
};

export default PredictionsList;
