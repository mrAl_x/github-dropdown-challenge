export function getGithubResults(searchQuery) {
	const token = '0a4b0a3fd9c9727b0a6fe9eb386e2c1a0cd20574';
	const apiUrl = `https://api.github.com/search/users?q=${searchQuery}&access_token=${token}`;

	try {
		return fetch(apiUrl)
			.then((res) => res.json()).then((data) => data);
	} catch (error) {
		console.error(error)
	}
}