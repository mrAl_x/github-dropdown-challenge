import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Helpers
import { getGithubResults } from './helpers.js';
// Styles
import styles from './TextBox.css';

class TextBox extends Component {
	constructor() {
		super();

		this.inputTimeOut = null;
	}

	render() {
		return (
			<input type="text"
				className={ styles.TextBox }
				onChange={ this.handleOnChange }
				onFocus={ () => this.updateFocus(true) }
				onBlur={ () => this.updateFocus(false) }
				onKeyDown={ this.handleKeyDown }
				placeholder="type a github username"
				autoFocus />
		);
	}

	/**
	 * Calls back a function to update the App's state to what the user typed in
	 * and calls the local updateSearchQuery function
	*/
	handleOnChange = (e) => {
		const searchQuery = e.target.value;

		this.props.updateSearchQuery(searchQuery);

		return this.updateSearchQuery(searchQuery);
	}

	/**
	 * Checks if there's an input value and if it contains more than 2 characters
	 * before making a call to the Github API
	*/
	updateSearchQuery = (searchQuery) => {
		clearTimeout(this.inputTimeOut);

		if (searchQuery && searchQuery.length >= 2) {
			return this.fetchSearchResults(searchQuery);
		}
	}

	/**
	 * Adds a timeout between keystrokes before making calls to the API
	*/
	fetchSearchResults = (searchQuery) => {
		const { updateResults } = this.props;

		this.inputTimeOut = setTimeout(() => {
			getGithubResults(searchQuery).then((res) => updateResults(res))
		}, 300);
	}

	/**
	 * Tell the parent component whether the input text is focused or not
	 * It adds a timeout so you can still click on the users links 🔨
	*/
	updateFocus = (isFocused) => {
		setTimeout(() => {
			this.props.updateInputFocus(isFocused);
		}, 100);
	}

	/**
	 * Tell the parent if the Up or Down arrow keys were pressed
	*/
	handleKeyDown = (e) => {
		if (e.key === 'ArrowDown' || e.key === 'ArrowUp') {
			this.props.arrowKeyDown(e.key);
		} else if (e.key === 'Enter') {
			this.handleSubmit();
		}
	}

	/**
	 * Submit the input value to the parent
	*/
	handleSubmit = () => {
		this.props.clickListItemLink();
	}
}

TextBox.propTypes = {
	searchResults: PropTypes.array,
	arrowKeyDown: PropTypes.func,
	updateResults: PropTypes.func.isRequired,
	updateInputFocus: PropTypes.func.isRequired,
	updateSearchQuery: PropTypes.func.isRequired,
	clickListItemLink: PropTypes.func.isRequired,
}

export default TextBox;
