import React from 'react';
import PropTypes from 'prop-types';

// Styles
import styles from './A11y.css'

const A11y = (props) => (
	<div className={ styles.A11y }>
		<span aria-live="assertive">
			{ `Found ${props.totalResults} results` }
		</span>
		<span aria-live="assertive">
			{`Go to ${props.currentHighlight}'s profile`}
		</span>
	</div>
);

A11y.propTypes = {
	totalResults: PropTypes.number,
	currentHighlight: PropTypes.string,
};

export default A11y;
