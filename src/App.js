import React, { Component } from 'react';

// Components
import TextBox from './components/text-box/TextBox.js';
import DummyContent from './components/dummy-content/DummyContent.js';
import Predictions from './components/predictions/Predictions';
import A11y from './components/a11y/A11y';

// Styles
import styles from './App.css';

// Resources
import ginettaLogoSvg from './shared/media/svg/ginetta-logo.svg'

class App extends Component {
	constructor() {
		super();
		this.state = {
			searchResults: null,
			isInputFocused: true,
			searchQuery: null,
			selectedListItemIndex: null,
			isLinkClicked: null,
			a11y: {
				totalResults: null,
				currentHighlight: null,
			},
		};
	}

	componentDidUpdate() {
		if (this.state.isLinkClicked) {
			return this.setState({
				isLinkClicked: false,
			});
		}
	}

	render() {
		const {
			searchResults,
			searchQuery,
			isInputFocused,
			selectedListItemIndex,
			isLinkClicked,
			a11y,
		} = this.state;

		return (
			<div className={ styles.App }>
				<img src={ ginettaLogoSvg }
					className={ styles.logo }
					alt="Ginetta" />
				<TextBox
					searchResults={ searchResults }
					updateResults={ this.updateResults }
					updateInputFocus={ this.updateInputFocus }
					arrowKeyDown={ this.handleArrowKeyPressed }
					updateSearchQuery={ this.updateSearchQuery }
					clickListItemLink={ this.clickListItemLink } />
				{
					searchResults && isInputFocused && <Predictions
						searchQuery={ searchQuery }
						searchResults={ searchResults }
						selectedListItemIndex={ selectedListItemIndex }
						isLinkClicked={ isLinkClicked }
						updateA11yCurrentHighlight={ (username) => this.updateA11yCurrentHighlight(username) } />
				}
				<DummyContent />
				<A11y
					totalResults={ a11y.totalResults }
					currentHighlight={ a11y.currentHighlight } />
			</div>
		);
	}

	/**
	 * Updates the input search query on the App level
	*/
	updateSearchQuery = (searchQuery) => {
		this.setState({
			searchQuery,
		});
	};

	/**
	 * Updates the App results list and the total number of results
	*/
	updateResults = (res) => {
		this.setState((prevState) => {
			return {
				searchResults: res.items,
				a11y: {
					totalResults: res.items.length,
					currentHighlight: prevState.a11y.currentHighlight,
				},
			};
		});
	}

	/**
	 * Update based on the text input focus/blur
	*/
	updateInputFocus = (isInputFocused) => {
		this.setState({
			isInputFocused,
		});
	}

	updateA11yCurrentHighlight = (username) => {
		this.setState((prevState) => {
			return {
				a11y: {
					totalResults: prevState.a11y.totalResults,
					currentHighlight: username,
				}
			};
		});
	}

	/**
	 * If the down arrow key is pressed, increment the index of the highlighted list item
	 * If the up arrow key is pressed, decrement the index of the highlighted list item
	 * The value can't go below 0 or above the total number of items on the results list
	*/
	handleArrowKeyPressed = (key) => {
		const { selectedListItemIndex, searchResults } = this.state;

		if (searchResults) {
			switch (key) {
				case 'ArrowDown':
					if (selectedListItemIndex >= 0 && selectedListItemIndex < (searchResults.length - 1)) {
						this.setState((prevState) => {
							if (prevState.selectedListItemIndex === null) {
								return {
									selectedListItemIndex: 0,
								}
							}

							return {
								selectedListItemIndex: prevState.selectedListItemIndex += 1,
							};
						});
					}

					break;
				case 'ArrowUp':
					if (selectedListItemIndex > 0 && selectedListItemIndex <= (searchResults.length - 1)) {
						this.setState((prevState) => {
							return {
								selectedListItemIndex: prevState.selectedListItemIndex -= 1,
							};
						});
					}

					break;
				default:
					break;
			}
		}
	}

	/**
	 * Update state if the user presses 'Enter' when a list item is highlighted
	*/
	clickListItemLink = () => {
		if (this.state.selectedListItemIndex >= 0 && this.state.selectedListItemIndex !== null) {
			this.setState({
				isLinkClicked: true,
			});
		}
	}
}

export default App;
