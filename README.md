# Github Dropdown Challenge
## Description
This is a simple application - using **ReactJS** - that allows users to search for Github users.

The build system is implemented using **Webpack**.

This app allows the user to:

* Look up users on Github
* Cycle through the users list of results with the keyboard
* Access each user's profile

#### Notes
You'll have to type **more than 2 characters** before the app makes an actual call to the Github API. This was not required, it was merely an implementation decision.

Also, the app is lacking a loading wheel (shameful, I know) so, after typing a few characters, try giving it a second or two before expecting any results.

## Running the project

### Installation
In order to run this app you'll need to install its dependencies - mainly ReactJS - via **NPM**:

`$ npm install`

### Build
To build the project you can run:

`$ npm run build`

The bundled project should be under the `/build` folder in the root of the project.

### Development Server
To startup a local server you can run:

`$ npm start`

## A11Y
This app is fully accessible so feel free to explore it with the keyboard.

Plus if you're running **MacOS**, try pressing *CMD + F5* to enable the screen reader and experience the audible goodness :)
